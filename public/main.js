const select = (select) => document.querySelector(select);
const form = document.querySelector('.form');

const title = select('#title');
const content = select('#content');
const thumbnail = select('#thumbnail');
const category = select('#category');
const message = select('.message');
const feature_content = select('.featureed-content');

console.log('show message' + message);


const exceptImageFiles = ['jpg', 'jpeg', 'png'];

const displayMessage = (text, color) => {
    message.style.visibility = 'visible';
    message.style.backgroundColor = color;
    message.innerText =  text;
}

const isCorrectMediaTypes = () => {
    if (thumbnail.value != '') {
        const extension = thumbnail.value.split('.').pop();
        if (!exceptImageFiles.includes(extension)) {
            alert('The file type is not valid');
        }
    }
};

const validateForm = () => {
    if (title.value == '' || content.value == '') {
      return displayMessage('Title or content cannot be empty', 'red');
    }
}
form.addEventListener('submit', async function (event) {
    // Validate our form
    const valid = validateForm();
    console.log(valid || 2);

   if(valid){ 
    // Submit this form
    const formData = new FormData(form);
    await postData(formData);
    console.log('send the form successful' + formData)
   }
});

// Create a function reset the form

const resetForm = () => {
    title.value = '';
    content.value = '';
    thumbnail.value = null;
    category.value = '0';
    feature_content.checked = false;

}
const postData = async (data) => {
    const result = await fetch('api/create', {
        method: 'POST',
        body: data,
    });
   
   if (result.ok){
      const response = await result.json()
      if (response.success){
        displayMessage(response.message, 'green');
      }

   }else{
        displayMessage(response.message, 'red');
   }
};
