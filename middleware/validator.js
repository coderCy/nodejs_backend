const {check,validationResult} = require('express-validator');
const expectedCategory = ['chinese','western'];


const validator = [
    check('title').trim().not().isEmpty().withMessage('Title is required'),
    check('content').trim().not().isEmpty().withMessage('Content is required'),
    check('category').isIn(expectedCategory).withMessage('Select atleast one category!')
]

const result = (req, res, next) => {
    const result = validationResult(req);
    const hasError = !result.isEmpty();

    if (hasError){
        const error = result.array()[0].msg;
        res.json({success:false, message: error}) 
    }
    next();

    return;
}

const validateFile = (req, res, next) => {
    const exceptedFileType = ['png','jpg','jpeg'];
    if (!req.file){
        return res.json({success: false, message: 'Food Image is required'})
    } 
    
    const fileExtension = req.file.mimetype.split('/').pop();
    
    if(!exceptedFileType.includes(fileExtension)){
       return res.json({success: false, message: 'Image file is not valid'})
    }
    next();

}

module.exports = {
    validator,
    result,
    validateFile
}