# nodejs_backend (`Food Soul`)
## Environment setup

`Languages NodeJs`

```` bash
-- Check Node Version First

$ npm node --version

$ npm init

$ npm install --save npm-install-global

// plugin
$ npm install --express express-validator fs npkill require router sharp
````

***
##  More About This Project
`Basic Features`

- This is the backend of the `Food Soul` Apps by using `NodeJs`

- Provide Create Restaurant News , Get Restaurants News and upload thumbnail

***
## Setup CICD
````yml
stages:
  - build
  - build-docker-image 

Build-nodejs-app:
  image: node:16.15.1-alpine
  stage: build
  script:
    - npm install
  artifacts:
    paths:
    - /home/gitlab-runner/

Build-docker-image:
  image: gitlab/dind
  services:
    - docker:dind
  stage: build-docker-image
  script:
    - docker info  
````
## Architecture
***
![Architecture](./Assets/architecture.png)


## Notes
***
- Before start the project you need to check your nodeJs setup
- This project is created By coderCy
- This project is for personal
- Version 1.0.0






