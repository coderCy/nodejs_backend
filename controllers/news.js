const News = require("../news/news");
const imageProcess = require("../util/imageProcess");

const news = new News();

const createNews = async (req,res) => {
  const id = news.createId();

  try{
      const imageName = await imageProcess(req,id);
      console.log('Name:' + imageName);
      news.create(req.body, id, imageName);
      res.json({success: true, message:'Post created Successfully'})
      
  }catch(error){
    res.json({success:false,message:'Something went wrong.'});
    console.log('Error while creating news', error.message)

  }

};

// Request Function

const getAllNews = async (req, res) => {
  try{
      const data = await news.getAll();
      res.json({success: true, news: data}) 
  }catch (error){
      res.json({
        success:false,
        message:'Internal Error',
      });
      console.log('Error while getting all news')
  }
}

const getSingleNews = async (req, res) => {
  try{
    const restaurnatNews = await news.getSingle(req.params.id);
       console.log('input data' + req.params.id) 

    if (!restaurnatNews){
       
      res.json({success:false,message:'Cannot find this restaurnatNews'}
  
      );
    }else{
      res.json({success:true,news:restaurnatNews})
    }

  }catch(error){

  }
}

const getNewsByCategory = async (req, res) => {
  try{
    const restaurnatNews = await news.getByCategory(req.params.category);
       console.log('input data ::' + req.params.category) ;
   

    if (!restaurnatNews) {
      res.json({success:false,message:'Cannot find this restaurnatNews'})
    }else{
      res.json({success:true,news:restaurnatNews})
    }
  }catch(error){
    
  }
}





module.exports = {
    createNews,
    getAllNews,
    getSingleNews,
    getNewsByCategory
}